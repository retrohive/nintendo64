# nintendo64

based on https://archive.org/details/no-intro_romsets (20210406-233941)

ROM not added :
```
❯ tree -hs --dirsfirst
.
├── [4.4M]  Cruis'n USA (USA) (Wii Virtual Console).zip
├── [ 52M]  Dinosaur Planet (USA) (Proto) (2000-12-01).zip
├── [2.7M]  Freak Boy (Unknown) (Proto).zip
├── [ 16M]  F-Zero X + The Legend of Zelda Ocarina of Time (USA) (Beta).zip
├── [ 27M]  Legend of Zelda, The - Majora's Mask (Europe) (En,Fr,De,Es) (Rev 1) (Wii Virtual Console).zip
├── [ 24M]  Mario Party 2 (Europe) (En,Fr,De,Es,It) (Wii Virtual Console).zip
├── [ 25M]  Mario Party 2 (Japan) (Wii Virtual Console).zip
├── [ 25M]  Mario Party 2 (USA) (Wii Virtual Console).zip
├── [ 14M]  Mario Tennis 64 (Japan) (Wii Virtual Console).zip
├── [ 14M]  Mario Tennis (Europe) (Wii Virtual Console).zip
├── [ 14M]  Mario Tennis (USA) (Wii Virtual Console).zip
├── [ 34M]  Ogre Battle 64 - Person of Lordly Caliber (Japan) (Rev 1) (Wii Virtual Console).zip
├── [ 34M]  Ogre Battle 64 - Person of Lordly Caliber (USA, Europe) (Rev 1) (Wii Virtual Console).zip
├── [ 16M]  Pokemon Puzzle League (Europe) (Wii Virtual Console).zip
├── [ 16M]  Pokemon Puzzle League (France) (Wii Virtual Console).zip
├── [ 16M]  Pokemon Puzzle League (Germany) (Wii Virtual Console).zip
├── [ 16M]  Pokemon Puzzle League (USA) (Wii Virtual Console).zip
├── [8.8M]  Pokemon Snap (Japan) (Rev 1) (Wii Virtual Console).zip
├── [8.7M]  Puyo Puyoon Party (Japan).zip
├── [9.8M]  Star Fox 64 (Japan) (Rev 1) (Wii Virtual Console).zip
├── [9.5M]  Viewpoint 2064 (Japan) (Proto).zip
└── [1.2M]  Whack 'n' Roll (Unknown) (Beta) (1998-02-05).zip

0 directories, 22 files
```
